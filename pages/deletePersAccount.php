<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Удалить л/сч</title>
    <?
    include "../includes/htmlLinks.php";
    ?>
</head>
<body>
    <?
    $filePath = "deletePersAccount.php?nameDB=osbb";
    include "../includes/header.php";
    ?>
    <div class="deletePersAccount">
        <h1 id="deletePATitle">Номер удаляемого лицевого счета:</h1><br><br>
        <form method="post" action="../includes/handlers/deletePersAccountHandler.php">
            <div class="deletePA">
                <input class="deletePAText" type="TEXT" name="deletePA"><br><br>
                <input type="hidden" name="nameTable" value="<?echo $_GET['tableName']?>">
                <input type="hidden" name="nameDB" value="<?echo $_GET['nameDB']?>">
                <div class="deletePAButtons">
                    <input class="clearButton" type="submit" value="">
                    <input class="sendButton" type="submit" value="">
                </div>
            </div>
        </form>
    </div>
</body>
</html>