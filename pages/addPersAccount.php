<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Добавить л/сч</title>
    <?
    include "../includes/htmlLinks.php";
    ?>
</head>
<body>
    <?
    $filePath = "addPersAccount.php?nameDB=osbb";

    include "../includes/header.php";
    ?>
    <div class="addPAWrap">
        <form method="post" action="../includes/handlers/addPersAccountHandler.php">
            <div class="addPAGeneralInf">
                <h1 class="addPATitles">Добавление лицевого счета</h1><br><br>
                <div class="addPAGeneralInfNames">
                    <span>№ лицевого счета:</span><br><br>
                    <span>Ф.И.О.:</span><br><br>
                    <span>Кол-во зарегистрированных:</span><br><br>
                    <span>Наличие субсидии:</span><br><br>
                    <span>Льгота:</span><br><br>
                    <span>Количество льготников:</span><br><br>
                </div>
                <div class="addPAGeneralInfValues">
                    <input class="addPAText" type="TEXT" name="addPATextPersAccount"><br><br>
                    <input class="addPAText" id="addPAGeneralInfName" type="TEXT" name="addPATextName"><br><br>
                    <input class="addPAText" type="TEXT" name="addPATextNumbOfReg"><br><br>
                    <input class="addPAText" type="TEXT" name="addPATextExSubs"><br><br>
                    <input class="addPAText" type="TEXT" name="addPATextBenefit"><br><br>
                    <input class="addPAText" type="TEXT" name="addPATextNumbOfBen"><br><br>
                </div>
            </div>
            <hr>
            <div class="addPAOldDeb">
                <h1 class="addPATitles">Задолженность состоянием на начало месяца</h1><br><br>
                <div class="addPAOldDebNames">
                    <span>Содержание дома:</span><br><br>
                    <span>Текущий и кап.ремонт:</span><br><br>
                </div>
                <div class="addPAOldDebValues">
                    <input type="hidden" name="nameTable" value="<?echo $_GET['tableName']?>">
                    <input type="hidden" name="nameDB" value="<?echo $_GET['nameDB']?>">
                    <input class="addPAText" type="TEXT" name="addPATextOldDebHousingServ"><br><br>
                    <input class="addPAText" type="TEXT" name="addPATextoldDebCurrOverhaul"><br><br>
                </div>
            </div>
            <hr>
            <div class="addPAHousingServ">
                <h1 class="addPATitles">Содержание дома</h1><br><br>
                <div class="addPAHousingServNames">
                    <span>Общая площадь:</span><br><br>
                    <span>Льготная площадь:</span><br><br>
                    <span>Начислено:</span><br><br>
                    <span>Компенсация субсидии:</span><br><br>
                    <span>Перерасчет:</span><br><br>
                    <span>Всего:</span><br><br>
                </div>
                <div class="addPAHousingServValues">
                    <input class="addPAText" type="TEXT" name="addPATextHousingServTotal"><br><br>
                    <input class="addPAText" type="TEXT" name="addPATextHousingServBen"><br><br>
                    <input class="addPAText" type="TEXT" name="addPATextHousingServCalc"><br><br>
                    <input class="addPAText" type="TEXT" name="addPATextHousingServCompSubs"><br><br>
                    <input class="addPAText" type="TEXT" name="addPATextHousingServRecalc"><br><br>
                    <input class="addPAText" type="TEXT" name="addPATextHousingServInAll"><br><br>
                </div>
            </div>
            <hr>
            <div class="addPACurrOverhaul">
                <h1 class="addPATitles">Текущий и кап.ремонт</h1><br><br>
                <div class="addPACurrOverhaulNames">
                    <span>Общая площадь:</span><br><br>
                    <span>Начислено:</span><br><br>
                    <span>Перерасчет:</span><br><br>
                    <span>Всего:</span><br><br>
                </div>
                <div class="addPACurrOverhaulValues">
                    <input class="addPAText" type="TEXT" name="addPATextCurrOverhaulTotal"><br><br>
                    <input class="addPAText" type="TEXT" name="addPATextCurrOverhaulCalc"><br><br>
                    <input class="addPAText" type="TEXT" name="addPATextCurrOverhaulRecalc"><br><br>
                    <input class="addPAText" type="TEXT" name="addPATextCurrOverhaulInAll"><br><br>
                </div>
            </div>
            <hr>
            <div class="addPAPaid">
                <h1 class="addPATitles">Оплачено</h1><br><br>
                <div class="addPAPaidNames">
                    <span>Содержание дома:</span><br><br>
                    <span>С теплом к людям:</span><br><br>
                    <span>Текущий и кап.ремонт:</span><br><br>
                    <span>Дата:</span><br><br>
                </div>
                <div class="addPAPaidValues">
                    <input class="addPAText" type="TEXT" name="addPATextPaidHousingServ"><br><br>
                    <input class="addPAText" type="TEXT" name="addPATextPaidWTWOTP"><br><br>
                    <input class="addPAText" type="TEXT" name="addPATextPaidCurrOverhaul"><br><br>
                    <input class="addPAText" type="TEXT" name="addPATextPaidDate"><br><br>
                </div>
            </div>
            <hr>
            <div class="addPANewDeb">
                <h1 class="addPATitles">Задолженность состоянием на конец месяца</h1><br><br>
                <div class="addPANewDebNames">
                    <span>Содержание дома:</span><br><br>
                    <span>Текущий и кап.ремонт:</span><br><br>
                </div>
                <div class="addPANewDebValues">
                    <input class="addPAText" type="TEXT" name="addPATextNewDebHousingServ"><br><br>
                    <input class="addPAText" type="TEXT" name="addPATextNewDebCurrOverhaul"><br><br>
                </div>
            </div>
            <hr>
            <div class="addPANumbStorey">
                <h1 class="addPATitles">Этаж</h1><br><br>
                <div class="addPANumbStoreyNames">
                    <span>Этаж:</span><br><br>
                </div>
                <div class="addPANumbStoreyValues">
                    <input class="addPAText" type="TEXT" name="addPATextNumbStorey"><br><br>

                </div>
            </div>
            <div class="addPAButtons">
                <input class="clearButton" type="submit" value="">
                <input class="sendButton" type="submit" value="">
            </div>
        </form>
</div>
</body>
</html>