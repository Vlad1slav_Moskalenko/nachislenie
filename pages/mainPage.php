<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Главная</title>
  <?include "../includes/htmlLinks.php";?>
</head>
<body>
  <?
  $filePath = "mainPage.php?nameDB=osbb";

  include_once "../includes/header.php";
  include_once '../includes/MainPage.php';
  ?>
  <div class="mainTable">
      <table>
        <div class="tableFixed">
              <tr>
                <th rowspan="3" >№ л/сч</th>
                <th rowspan="3" >П.І.Б.</th>
                <th rowspan="3" class="verticalText">кількість зареєстров.</th>
                <th rowspan="3" class="verticalText">наявність субсидії</th>
                <th rowspan="3" class="verticalText">пільга %</th>
                <th rowspan="3" class="verticalText">кількість пільговиків</th>
                <th rowspan="2" colspan="2">забогованість станом на <? echo $mounths[count($mounths) - 2];?></th>
                <th colspan="7" >утримання будинку</th>
                <th colspan="5" >поточний та кап.ремонт</th>
                <th colspan="4">сплачено, грн</th>
                <th rowspan="2" colspan="2">заборгованість станом на <?echo end($mounths);?></th>
                <th rowspan="3">№ л/сч</th>
                <th rowspan="3" class="verticalText">№ єтажа</th>
              </tr>
              <tr>
                <th>S</th>
                <th>S</th>
                <th>тариф</th>
                <th>нарахов</th>
                <th rowspan="2" class="verticalText">компенс. субсидії</th>
                <th rowspan="2" class="verticalText smallerFont">перерахунок</th>
                <th>всього</th>
                <th>S</th>
                <th>тариф</th>
                <th>нарахов</th>
                <th rowspan="2" class="verticalText smallerFont">перерахунок</th>
                <th>всього</th>
                <th rowspan="2" class="verticalText">утрим. будинку</th>
                <th rowspan="2" class="verticalText smallerFont">з теплом до людей</th>
                <th rowspan="2" class="verticalText">поточн. та кап.рем.</th>
                <th rowspan="2">дата</th>
              </tr>
              <tr>
                <th class="smallerFont">утрим. будинку</th>
                <th class="smallerFont">поточн. та кап.ремонт</th>
                <th>загальна</th>
                <th>пільгова</th>
                <th>грн</th>
                <th>грн</th>
                <th>грн</th>
                <th>загальна</th>
                <th>грн</th>
                <th>грн</th>
                <th>грн</th>
                <th class="smallerFont">утрим. будинку</th>
                <th class="smallerFont">поточн. та кап.ремонт</th>
              </tr>
        </div>
          <?
          $MP = new MainPage();
          echo $MP->generateHtmlAllMainTable();
          ?>
    </table>
  </div>
</body>
</html>
