<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Ввод оплат</title>
    <?
    include "../includes/htmlLinks.php";
    ?>
</head>
<body>
    <?
    $filePath = "paymentEntry.php?nameDB=osbb";

    include "../includes/header.php";
    ?>
    <div class="search">
        <form name="searchForm" method="GET">
            <input type="hidden" name="nameDB" value="<?echo $_GET['nameDB']?>">
            <input type="hidden" name="tableName" value="<?echo $_GET['tableName']?>">
            <input class="searchInputText" type="TEXT" name="searchRequest">
            <input class="searchButton" type="submit" value="Поиск">
            <br>
            <div class="searchRadioPanel">
                <input class="searchInputRadio" type="radio" name="searchType" checked value="PA">Лицевой счет
                <input class="searchInputRadio" type="radio" name="searchType" value="name">Ф.И.О.
            </div>
        </form>
    </div>
    <hr id="searchHrLine">
    <div class="paymentEntryPanel">
        <div class="paymentEntryPanelNames">
            <span>Лицевой счет:</span><br><br>
            <span>Содерж. дома:</span><br><br>
            <span>Кап.ремонт:</span><br><br>
            <span>З теплом до людей:</span><br><br>
            <span>Дата:</span><br><br>
        </div>
        <form method="post" action="../includes/handlers/paymentEntryHandler.php">
            <div class="paymentEntryPanelInput">
                <input class="paymentEntryText" type="TEXT" name="paymentEntryName"><br><br>
                <input class="paymentEntryText" type="TEXT" name="paymentEntryHousingServ"><br><br>
                <input class="paymentEntryText" type="TEXT" name="paymentEntryCurrOverhaul"><br><br>
                <input class="paymentEntryText" type="TEXT" name="paymentEntryWTWOTP"><br><br>
                <input class="paymentEntryText" type="TEXT" name="paymentEntryDate"><br><br>
                <input type="hidden" name="nameTable" value="<?echo $_GET['tableName']?>">
                <input type="hidden" name="nameDB" value="<?echo $_GET['nameDB']?>">

            </div>
            <div class="paymentEntryButtons">
                <input class="clearButton" type="submit" value="">
                <input class="sendButton" type="submit" value="">
            </div>
        </form>
    </div>

    <table id="paymentEntryTable">
        <tr>
            <th rowspan="2">№ л/сч</th>
            <th rowspan="2">П.І.Б.</th>
            <th colspan="2">заборгованність станом на 2015.01</th>
            <th rowspan="2">нарахов. утрим. буд. грн</th>
            <th rowspan="2">нарахов. поточн. та кап.рем. грн</th>
            <th colspan="4">сплачено, грн</th>
            <th colspan="2">заборгованність станом на 2015.02</th>
        </tr>
        <tr>
            <th>утрим. будинку</th>
            <th>поточн. та кап. ремонт</th>
            <th>утрим. будинку</th>
            <th>з теплом до людей</th>
            <th>поточн. та кап. ремонт</th>
            <th>дата</th>
            <th>утрим. будинку</th>
            <th>поточн. та кап. ремонт</th>
        </tr>
        <?
        include_once '../includes/PaymentEntry.php';

        $PE = new PaymentEntry();
        echo $PE->generateHtmlPaymentEntryTable();
        ?>
    </table>


</body>
</html>
