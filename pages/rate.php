<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Изменить тариф</title>
    <?
    include_once "../includes/htmlLinks.php";
    ?>
</head>
<body>
    <?
    $filePath = "rate.php?nameDB=osbb";

    include_once "../includes/header.php";
    include_once "../includes/Rate.php";

    $rate = new Rate();

    ?>
    <div class="currRate">
        <h1 id="currRateTitle">Текущий тариф</h1>
        <div class="oldHousingServRate">
            <span id="oldHousingServRateTitle">Содержание дома:</span><br><br>
            <div class="oldHousingServRateNames">
                <span>1 этаж:</span><br><br>
                <span>2+ этаж:</span><br>
            </div>
            <div class="oldHousingServRateValues">
                <span><? echo $rate->housingServRateFirstStorey." грн";?></span><br><br>
                <span><? echo $rate->housingServRateNotFirstStorey." грн";?></span><br>
            </div>
        </div>
        <div class="oldCurrOverhaulRate">
            <span id="oldCurrOverhaulRateTitle">Текущий и кап. ремонт:</span><br><br>
            <div class="oldCurrOverhaulRateNames">
                <span>1 этаж:</span><br><br>
                <span>2+ этаж:</span><br>
            </div>
            <div class="oldCurrOverhaulRateValues">
                <span><? echo $rate->currOverhaulRateFirstStorey." грн";?></span><br><br>
                <span><? echo $rate->currOverhaulRateNotFirstStorey." грн";?></span><br>
            </div>
        </div>
    </div>

    <div class="newRate">
        <h1 id="newRateTitle">Новый тариф</h1>
        <form method="post" action="../includes/handlers/rateHandler.php">
            <div class="newHousingServRate">
                <div class="newHousingServRateNames">
                    <span>1 этаж:</span><br><br>
                    <span>2+ этаж:</span><br>
                </div>
                <div class="newHousingServRateValues">
                    <input class="rateEntryText" type="TEXT" name="rateHousingServEntry1"><br><br>
                    <input class="rateEntryText" type="TEXT" name="rateHousingServEntry2"><br><br>
                </div>
            </div>
            <div class="newCurrOverhaulRate">
                <div class="newCurrOverhaulRateNames">
                    <span>1 этаж:</span><br><br>
                    <span>2+ этаж:</span><br>
                </div>
                <div class="newCurrOverhaulRateValues">
                    <input class="rateEntryText" type="TEXT" name="rateCurrOverhaulEntry1"><br><br>
                    <input class="rateEntryText" type="TEXT" name="rateCurrOverhaulEntry2"><br><br>
                    <input type="hidden" name="nameTable" value="<?echo $_GET['tableName']?>">
                    <input type="hidden" name="nameDB" value="<?echo $_GET['nameDB']?>">
                </div>
            </div>
            <div class="rateButtons">
                <input class="clearButton" type="submit" value="">
                <input class="sendButton" type="submit" value="">
            </div>
        </form>
    </div>

</body>
</html>