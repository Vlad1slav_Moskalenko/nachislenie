<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Изменить л/сч</title>
    <?
    include "../includes/htmlLinks.php";
    ?>
</head>
<body>
    <?
    $filePath = "changePersAccount.php?nameDB=osbb";

    include "../includes/header.php";
    ?>
    <div class="changePersAccountWrap">
        <form method="post">
            <div class="changePAGeneralInf">
                <h1 class="changePATitles">Изменение лицевого счета</h1><br><br>
                <div class="changePAGeneralInfNames">
                    <span>№ изменяемого л/сч:</span><br><br>
                    <span>Новый № л/сч:</span><br><br>
                    <span>Ф.И.О.:</span><br><br>
                    <span>Кол-во зарегистрированных:</span><br><br>
                    <span>Наличие субсидии:</span><br><br>
                    <span>Льгота:</span><br><br>
                    <span>Количество льготников:</span><br><br>
                </div>
                <div class="changePAGeneralInfValues">
                    <input class="addPAText" type="TEXT" name="changePATextOldPersAccount"><br><br>
                    <input class="addPAText" type="TEXT" name="changePATextNewPersAccount"><br><br>
                    <input class="addPAText" id="addPAGeneralInfName" type="TEXT" name="changePATextName"><br><br>
                    <input class="addPAText" type="TEXT" name="changePATextNumbOfReg"><br><br>
                    <input class="addPAText" type="TEXT" name="changePATextExSubs"><br><br>
                    <input class="addPAText" type="TEXT" name="changePATextBenefit"><br><br>
                    <input class="addPAText" type="TEXT" name="changePATextNumbOfBen"><br><br>
                </div>
            </div>
            <hr>
            <div class="changePAHousingServ">
                <h1 class="changePATitles">Содержание дома</h1><br><br>
                <div class="changePAHousingServNames">
                    <span>Общая площадь:</span><br><br>
                    <span>Льготная площадь:</span><br><br>
                    <span>Компенсация субсидии:</span><br><br>
                    <span>Перерасчет:</span><br><br>
                </div>
                <div class="changePAHousingServValues">
                    <input class="addPAText" type="TEXT" name="changePATextHousingServTotal"><br><br>
                    <input class="addPAText" type="TEXT" name="changePATextHousingServBen"><br><br>
                    <input class="addPAText" type="TEXT" name="changePATextHousingServCompSubs"><br><br>
                    <input class="addPAText" type="TEXT" name="changePATextHousingServRecalc"><br><br>
                </div>
            </div>
            <hr>
            <div class="changePACurrOverhaul">
                <h1 class="changePATitles">Текущий и кап.ремонт</h1><br><br>
                <div class="changePACurrOverhaulNames">
                    <span>Общая площадь:</span><br><br>
                    <span>Перерасчет:</span><br><br>
                </div>
                <div class="changePACurrOverhaulValues">
                    <input class="addPAText" type="TEXT" name="changePATextCurrOverhaulTotal"><br><br>
                    <input class="addPAText" type="TEXT" name="changePATextCurrOverhaulRecalc"><br><br>
                </div>
            </div>
            <div class="changePAButtons">
                <input class="clearButton" type="submit" value="">
                <input class="sendButton" type="submit" value="">
            </div>
        </form>
    </div>

</body>
</html>