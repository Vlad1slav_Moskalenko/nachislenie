<?php

class config
{
    public static $config = array(
                                'db' => array(
                                            'host' => 'localhost',
                                            'username' => 'root',
                                            'password' => '',
                                            'charset' => 'utf8'
                                ),
                                'titles' => array(
                                    'osbb' => 'ОСББ Комарова 25А',
                                    'gsk-7' => 'ЖСК-7 Радиоприбор'
                                )

    );
}
