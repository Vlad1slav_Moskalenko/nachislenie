<?php
/**
 * Created by PhpStorm.
 * User: Moskalenko
 * Date: 23.09.2017
 * Time: 14:18
 */

include_once 'DB.php';

class MainPage extends DB
{
    public function getAllMainTable()
    {
        if ($this->connection) {
            parent::getConnection();
        }

        if (!$_GET['tableName'])
            $tableName = end(parent::getNamesAllTables());
        else
            $tableName = $_GET['tableName'];

        $result = array();
        foreach (($this->connection->query("SELECT * FROM `$tableName` ORDER BY persAccount;")) as $row) {
            $result[] = array_values($row);
        }

        return $result;
    }

    public function generateHtmlAllMainTable()
    {
        $arrayAllMainTable = $this->getAllMainTable();
        $htmlAllMainTable = '';
        $mainTableHeader = '<tr>
                <th rowspan="3" >№ л/сч</th>
                <th rowspan="3" >П.І.Б.</th>
                <th rowspan="3" class="verticalText">кількість зареєстров.</th>
                <th rowspan="3" class="verticalText">наявність субсидії</th>
                <th rowspan="3" class="verticalText">пільга %</th>
                <th rowspan="3" class="verticalText">кількість пільговиків</th>
                <th rowspan="2" colspan="2">забогованість станом на 2015.01</th>
                <th colspan="7" > утримання будинку</th>
                <th colspan="5" >поточний та кап.ремонт</th>
                <th colspan="4">сплачено, грн</th>
                <th rowspan="2" colspan="2">заборгованість станом на 2015.02</th>
                <th rowspan="3">№ л/сч</th>
                <th rowspan="3" class="verticalText">№ єтажа</th>
              </tr>
              <tr>
                <th>S</th>
                <th>S</th>
                <th>тариф</th>
                <th>нарахов</th>
                <th rowspan="2" class="verticalText">компенс. субсидії</th>
                <th rowspan="2" class="verticalText smallerFont">перерахунок</th>
                <th>всього</th>
                <th>S</th>
                <th>тариф</th>
                <th>нарахов</th>
                <th rowspan="2" class="verticalText smallerFont">перерахунок</th>
                <th>всього</th>
                <th rowspan="2" class="verticalText">утрим. будинку</th>
                <th rowspan="2" class="verticalText smallerFont">з теплом до людей</th>
                <th rowspan="2" class="verticalText">поточн. та кап.рем.</th>
                <th rowspan="2">дата</th>
              </tr>
              <tr>
                <th class="smallerFont">утрим. будинку</th>
                <th class="smallerFont">поточн. та кап.ремонт</th>
                <th>загальна</th>
                <th>пільгова</th>
                <th>грн</th>
                <th>грн</th>
                <th>грн</th>
                <th>загальна</th>
                <th>грн</th>
                <th>грн</th>
                <th>грн</th>
                <th class="smallerFont">утрим. будинку</th>
                <th class="smallerFont">поточн. та кап.ремонт</th>
              </tr>';

        for ($i = 0; $i < count($arrayAllMainTable); $i++) {
            if ($i == 35) {
                $htmlAllMainTable .= $mainTableHeader;
            }
            $htmlAllMainTable .= "<tr class='tableContent'>";
            for ($j = 1; $j < 28; $j++){
                if ($j != 27){
                    $htmlAllMainTable .= "<td>" . $arrayAllMainTable[$i][$j] . "</td>";
                }
                else {
                    $htmlAllMainTable .= "<td>" . $arrayAllMainTable[$i][1] . "</td>";      //Повторная вставка № л/сч
                    $htmlAllMainTable .= "<td>" . $arrayAllMainTable[$i][$j] . "</td>";
                }
            }
            $htmlAllMainTable .= "</tr>";
        }

        return $htmlAllMainTable;
    }
}