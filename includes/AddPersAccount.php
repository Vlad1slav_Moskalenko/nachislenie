<?php
/**
 * Created by PhpStorm.
 * User: Moskalenko
 * Date: 25.09.2017
 * Time: 19:09
 */

include_once "DB.php";

class AddPersAccount extends DB
{
    public function addPersAccount($data, $nameTable)
    {
        $sqlQuery = $this->generateQuery($data, $nameTable);


        foreach ($data as $key =>$value) {
            if($value)
                $newData[":".$key] = $value;
        }
        unset($data);

        try {
            $stmt = $this->connection->prepare($sqlQuery);
            $stmt->execute($newData);
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function generateQuery($data, $nameTable)
    {
        $sqlQuery = 'INSERT INTO `'.$nameTable;
        $sqlQuery .= '` (';
        foreach ($data as $key => $value) {
            if($value)
                $sqlQuery .= (' '.$key.',');
        }
        $sqlQuery = substr($sqlQuery, 0, -1);
        $sqlQuery .= ') VALUES (';
        foreach ($data as $key => $value) {
            if($value) {
                    $sqlQuery .= (' :' . $key . ',');
            }
        }
        $sqlQuery = substr($sqlQuery, 0, -1);
        $sqlQuery .= ')';

        return $sqlQuery;
    }

    public function __construct($manualSelection = false, $nameDB = '')
    {
        parent::__construct($manualSelection, $nameDB);
    }
}