<?php
/**
 * Created by PhpStorm.
 * User: Moskalenko
 * Date: 26.09.2017
 * Time: 18:38
 */

include_once "DB.php";

class DeletePersAccount extends DB
{
    public function deletePA($nameTable, $persAccount)
    {
        $sqlQuery = "DELETE FROM `".$nameTable."` WHERE persAccount = :persAccount";

        try {
            $stmt = $this->connection->prepare($sqlQuery);
            $stmt->execute([':persAccount' => $persAccount]);
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }

    }

    public function __construct($manualSelection = false, $nameDB = '')
    {
        parent::__construct($manualSelection, $nameDB);
    }
}