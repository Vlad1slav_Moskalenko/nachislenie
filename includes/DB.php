<?php
/**
 * Created by PhpStorm.
 * User: Moskalenko
 * Date: 23.09.2017
 * Time: 14:03
 */

include_once 'config.php';

class DB
{
    protected $connection;

    public function getConnection($manualSelection = false, $nameDB = '')
    {
        if (!$manualSelection)
            if ($this->connection) {
                return;
            }

        $host = config::$config['db']['host'];
        $username = config::$config['db']['username'];
        $password = config::$config['db']['password'];
        $charset = config::$config['db']['charset'];

        if (!$manualSelection)
            $nameDB = $_GET['nameDB'];
        else
            $nameDB = func_get_arg(1);

        $dsn = "mysql:host=$host;dbname=$nameDB;charset=$charset";

        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];

        try {
            $this->connection = new PDO($dsn, $username, $password, $opt);
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function getNamesAllTables()
    {
        if (!$this->connection) {
            $this->getConnection();
        }

        $result = array();
        foreach (($this->connection->query('SHOW TABLES')) as $row) {
            $result[] = array_values($row)[0];
        }

        return $result;
    }

    public function __construct($manualSelection = false, $nameDB = '')
    {
        $this->getConnection($manualSelection, $nameDB);
    }
}