<?php
/**
 * Created by PhpStorm.
 * User: Moskalenko
 * Date: 24.09.2017
 * Time: 20:11
 */

include_once "DB.php";

class Rate extends DB
{
    public $housingServRateFirstStorey;
    public $housingServRateNotFirstStorey;
    public $currOverhaulRateFirstStorey;
    public $currOverhaulRateNotFirstStorey;

    private $tableName;

    public function getRate()
    {
        if (!$this->connection) {
            parent::getConnection();
        }

        //$tableName = $_GET['tableName'];

        $sqlQueryFirstStorey = "SELECT housingServRate, currOverhaulRate FROM `$this->tableName` WHERE numbStorey = 1 LIMIT 1";
        $sqlQueryNotFirstStorey = "SELECT housingServRate, currOverhaulRate FROM `$this->tableName` WHERE numbStorey = 2 LIMIT 1";

        try {
            foreach (($this->connection->query($sqlQueryFirstStorey)) as $row) {
                $resultFirstStorey = array_values($row);
            }
            foreach (($this->connection->query($sqlQueryNotFirstStorey)) as $row) {
                $resultNotFirstStorey = array_values($row);
            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }

        $this->housingServRateFirstStorey = $resultFirstStorey[0];
        $this->currOverhaulRateFirstStorey = $resultFirstStorey[1];

        $this->housingServRateNotFirstStorey = $resultNotFirstStorey[0];
        $this->currOverhaulRateNotFirstStorey = $resultNotFirstStorey[1];
    }

    public function setRate($newRatesFirstStorey, $newRatesNotFirstStorey, $nameTable)
    {
        $sqlQueryFirstStorey = "UPDATE `" . $nameTable . "` SET housingServRate = :housingServRateFirstStorey, 
        currOverhaulRate = :currOverhaulRateFirstStorey WHERE numbStorey = 1";
        $sqlQueryNotFirstStorey = "UPDATE `" . $nameTable . "` SET housingServRate = :housingServRateNotFirstStorey, 
        currOverhaulRate = :currOverhaulRateNotFirstStorey WHERE numbStorey > 1";

        try {
            $stmt = $this->connection->prepare($sqlQueryFirstStorey);
            $stmt->execute($newRatesFirstStorey);

            $stmt = $this->connection->prepare($sqlQueryNotFirstStorey);
            $stmt->execute($newRatesNotFirstStorey);
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function __construct($getRate = true, $manualSelection = false, $nameDB = '', $nameTable = '')
    {
        parent::__construct($manualSelection, $nameDB);
        if(!$manualSelection)
            $this->tableName = $_GET['tableName'];
        else
            $this->tableName = $nameTable;

        if ($getRate == true)
            $this->getRate();


    }
}