<?php
/**
 * Created by PhpStorm.
 * User: Moskalenko
 * Date: 23.09.2017
 * Time: 14:34
 */

include_once 'DB.php';

class PaymentEntry extends DB
{
    public function getPaymentEntryTable()
    {
        if (!$this->connection) {
            parent::getConnection();
        }

        $tableName = $_GET['tableName'];

        $result = array();
        $sqlQuery = "SELECT persAccount, name, oldDebHousingServ, oldDebCurrOverhaul, 
        housingServCalc, currOverhaulCalc, paidHousingServ, paidWTWOTP, paidCurrOverhaul, 
        paidDate, newDebHousingServ, newDebCurrOverhaul FROM `$tableName` WHERE ";
        $key = '';

        if ($_GET['searchType'] === "PA")
            $key .= 'persAccount=';

        if ($_GET['searchType'] === "name")
            $key .= 'name=';

        $key .= '\''.$_GET['searchRequest'].'\'';
        $sqlQuery .= $key;

        try {
            foreach (($this->connection->query($sqlQuery)) as $row) {
                $result[] = array_values($row);
            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }

        return $result;
    }

    public function generateHtmlPaymentEntryTable()
    {
        $arrayPaymentEntryTable = $this->getPaymentEntryTable();
        $htmlPaymentEntryTable = '';


        for ($i = 0; $i < count($arrayPaymentEntryTable); $i++) {
            $htmlPaymentEntryTable .= "<tr class='tableContent'>";
            for ($j = 0; $j < 12; $j++){
                $htmlPaymentEntryTable .= "<td>" . $arrayPaymentEntryTable[$i][$j] . "</td>";
            }
            $htmlPaymentEntryTable .= "</tr>";
        }

        return $htmlPaymentEntryTable;
    }

    public function getPayment($persAccount, $nameTable)
    {
        $sqlQuery =  "SELECT paidHousingServ, paidWTWOTP, paidCurrOverhaul, benefit, oldDebHousingServ, housingServTotal,
                     housingServBen, housingServRate, housingServCompSubs, housingServRecalc, oldDebCurrOverhaul,
                     currOverhaulTotal, currOverhaulRate, currOverhaulRecalc, paidDate FROM `$nameTable` 
                     WHERE persAccount=";
        $sqlQuery .= "'$persAccount'";

        try {
            foreach (($this->connection->query($sqlQuery)) as $row) {
                $result = $row;
            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            exit;
        }

        return $result;
    }

    public function updatePayment($newPayment, $oldPayment, $nameTable) {

        $sqlQuery = "UPDATE `" . $nameTable . "` SET paidHousingServ = :paidHousingServ, paidCurrOverhaul = :paidCurrOverhaul, 
        paidWTWOTP = :paidWTWOTP, paidDate = :paidDate WHERE persAccount = :persAccount;";

        try {
            $stmt = $this->connection->prepare($sqlQuery);
            $stmt->execute($newPayment);
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }

        $this->recalcDB($newPayment, $oldPayment, $nameTable);
    }

    public function recalcDB($newPayment, $oldPayment, $nameTable)
    {
        self::recalcHousingServ($newPayment, $oldPayment, $nameTable);
        self::recalcCurrOverhaul($newPayment, $oldPayment, $nameTable);
    }

    private function recalcHousingServ($newPayment, $oldPayment, $nameTable)
    {
        $persAccount = $newPayment['persAccount'];
        $paidHousingServ = $newPayment['paymentEntryHousingServ'];
        $paidWTWOTP = $newPayment['paymentEntryWTWOTP'];

        $benefit = $oldPayment['benefit'];
        $oldDebHousingServ = $oldPayment['oldDebHousingServ'];
        $housingServTotal = $oldPayment['housingServTotal'];
        $housingServBen = $oldPayment['housingServBen'];
        $housingServRate = $oldPayment['housingServRate'];
        $housingServCompSubs = $oldPayment['housingServCompSubs'];
        $housingServRecalc = $oldPayment['housingServRecalc'];

        switch ((int)$benefit) {
            case 25:
               $benefit = 0.75;
               break;
            case 50:
               $benefit = 0.5;
               break;
            case 75:
               $benefit = 0.25;
               break;
            case 100:
               $benefit = 0.0;
               break;
            default:
               $benefit = 1.0;
        }

        $housingServCalc = ($housingServTotal - $housingServBen) * $housingServRate +($housingServBen * $housingServRate * $benefit);
        $housingServInAll = $housingServCalc + $housingServCompSubs + $housingServRecalc;
        $newDebHousingServ = $oldDebHousingServ + $housingServInAll - $paidHousingServ - $paidWTWOTP;

        $newHousingServ = array(
            ':housingServCalc'      => $housingServCalc,
            ':housingServInAll'     => $housingServInAll,
            ':newDebHousingServ'    => $newDebHousingServ,
            ':persAccount'          => $persAccount
        );

        $sqlQuery = "UPDATE `" . $nameTable . "` SET housingServCalc = :housingServCalc, housingServInAll = :housingServInAll,
        newDebHousingServ = :newDebHousingServ WHERE persAccount = :persAccount";

        try {
            $stmt = $this->connection->prepare($sqlQuery);
            $stmt->execute($newHousingServ);
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }

    }

    private function recalcCurrOverhaul($newPayment, $oldPayment, $nameTable)
    {
        $persAccount = $newPayment['persAccount'];
        $paidCurrOverhaul = $newPayment['paymentEntryCurrOverhaul'];

        $oldDebCurrOverhaul = $oldPayment['oldDebCurrOverhaul'];
        $currOverhaulTotal = $oldPayment['currOverhaulTotal'];
        $currOverhaulRate = $oldPayment['currOverhaulRate'];
        $currOverhaulRecalc = $oldPayment['currOverhaulRecalc'];

        $currOverhaulCalc = $currOverhaulTotal * $currOverhaulRate;
        $currOverhaulInAll = $currOverhaulCalc + $currOverhaulRecalc;
        $newDebCurrOverhaul = $oldDebCurrOverhaul + $currOverhaulInAll - $paidCurrOverhaul;

        $newCurrOverhaul = array(
            ':currOverhaulCalc'      => $currOverhaulCalc,
            ':currOverhaulInAll'     => $currOverhaulInAll,
            ':newDebCurrOverhaul'    => $newDebCurrOverhaul,
            ':persAccount'           => $persAccount
        );

        $sqlQuery = "UPDATE `" . $nameTable . "` SET currOverhaulCalc = :currOverhaulCalc, currOverhaulInAll = :currOverhaulInAll,
        newDebCurrOverhaul = :newDebCurrOverhaul WHERE persAccount = :persAccount";

        try {
            $stmt = $this->connection->prepare($sqlQuery);
            $stmt->execute($newCurrOverhaul);
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}