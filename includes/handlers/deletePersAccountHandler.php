<?php
/**
 * Created by PhpStorm.
 * User: Moskalenko
 * Date: 26.09.2017
 * Time: 18:38
 */

include_once "../DeletePersAccount.php";

$nameTable = $_POST['nameTable'];
$deletePA = $_POST['deletePA'];

$delPA = new DeletePersAccount(true, $_POST['nameDB']);
$delPA->deletePA($nameTable, $deletePA);

header("Location: ".$_SERVER['HTTP_REFERER']);