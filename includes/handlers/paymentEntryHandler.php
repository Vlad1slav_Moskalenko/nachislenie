<?php
/**
 * Created by PhpStorm.
 * User: Moskalenko
 * Date: 23.09.2017
 * Time: 15:22
 */

include_once "../PaymentEntry.php";

$newPayment = array(
            'persAccount' => $_POST['paymentEntryName'],
            'paidHousingServ' => $_POST['paymentEntryHousingServ'],
            'paidCurrOverhaul' => $_POST['paymentEntryCurrOverhaul'],
            'paidWTWOTP' => $_POST['paymentEntryWTWOTP'],
            'paidDate' => $_POST['paymentEntryDate']
        );

$PE = new PaymentEntry(true, $_POST['nameDB']);
$oldPayment = $PE->getPayment($newPayment['persAccount'], $_POST['nameTable']);

// Приведение $newPayment['paidDate'] к виду 'дд.мм.гггг'
preg_match('/^(\d\d).(\d\d).(\d\d\d\d)\w*/is', $newPayment['paidDate'], $matches);
$newPayment['paidDate'] = $matches[1] . '.' . $matches[2] . '.' . $matches[3];


// Проверка введенной даты на наличие ошибок (число не больше 31, месяц не больше 12)
if($matches[1] > 31 || $matches[2] > 12) {
    echo "<br>Error in the field 'Date' (".$newPayment['paidDate'].")";
    exit;
}

//  Если в БД хранится дата по умолчанию (00.00.000) или равная новой введенной дате, $newPayment['paidDate'] в таком виде
//и отправляется в БД, в противном случае (при двух оплатах и более в одном месяце) к числу новой оплаты, после запятой,
//дописывается старая дата (прим. '24,01.09.2017').
if ($oldPayment['paidDate'] !== "00.00.0000" && $oldPayment['paidDate'] !== $newPayment['paidDate']) {
    $newPayment['paidDate'] = $matches[1] . ',' . $oldPayment['paidDate'];
}


$newPayment['paidHousingServ']  = $oldPayment['paidHousingServ']    + $newPayment['paidHousingServ'];
$newPayment['paidCurrOverhaul'] = $oldPayment['paidCurrOverhaul']   + $newPayment['paidCurrOverhaul'];
$newPayment['paidWTWOTP']       = $oldPayment['paidWTWOTP']         + $newPayment['paidWTWOTP'];

$PE->updatePayment($newPayment, $oldPayment, $_POST['nameTable']);

header("Location: ".$_SERVER['HTTP_REFERER']);

