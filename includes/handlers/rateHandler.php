<?php
/**
 * Created by PhpStorm.
 * User: Moskalenko
 * Date: 24.09.2017
 * Time: 21:30
 */

include_once '../Rate.php';

$newRatesFirstStorey = array(
    ':housingServRateFirstStorey'       => $_POST['rateHousingServEntry1'],
    ':currOverhaulRateFirstStorey'      => $_POST['rateCurrOverhaulEntry1']
);

$newRatesNotFirstStorey = array(
    ':housingServRateNotFirstStorey'    => $_POST['rateHousingServEntry2'],
    ':currOverhaulRateNotFirstStorey'   => $_POST['rateCurrOverhaulEntry2']
);

$rateObj = new Rate(false, true, $_POST['nameDB']);
$rateObj->setRate($newRatesFirstStorey, $newRatesNotFirstStorey, $_POST['nameTable']);

header("Location: ".$_SERVER['HTTP_REFERER']);
