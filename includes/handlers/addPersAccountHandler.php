<?php
/**
 * Created by PhpStorm.
 * User: Moskalenko
 * Date: 25.09.2017
 * Time: 19:10
 */

include_once "../AddPersAccount.php";
include_once "../Rate.php";



$data = array(
        'persAccount'          => $_POST['addPATextPersAccount'],
        'name'                 => "'".$_POST['addPATextName']."'",
        'numbOfReg'            => $_POST['addPATextNumbOfReg'],
        'exSubs'               => $_POST['addPATextExSubs'],
        'benefit'              => $_POST['addPATextBenefit'],
        'numbOfBen'            => $_POST['addPATextNumbOfBen'],
        'oldDebHousingServ'    => $_POST['addPATextOldDebHousingServ'],
        'oldDebCurrOverhaul'   => $_POST['addPATextoldDebCurrOverhaul'],
        'housingServTotal'     => $_POST['addPATextHousingServTotal'],
        'housingServBen'       => $_POST['addPATextHousingServBen'],
        'housingServRate'      => '',
        'housingServCalc'      => $_POST['addPATextHousingServCalc'],
        'housingServCompSubs'  => $_POST['addPATextHousingServCompSubs'],
        'housingServRecalc'    => $_POST['addPATextHousingServRecalc'],
        'housingServInAll'     => $_POST['addPATextHousingServInAll'],
        'currOverhaulTotal'    => $_POST['addPATextCurrOverhaulTotal'],
        'currOverhaulRate'     => '',
        'currOverhaulCalc'     => $_POST['addPATextCurrOverhaulCalc'],
        'currOverhaulRecalc'   => $_POST['addPATextCurrOverhaulRecalc'],
        'currOverhaulInAll'    => $_POST['addPATextCurrOverhaulInAll'],
        'paidHousingServ'      => $_POST['addPATextPaidHousingServ'],
        'paidWTWOTP'           => $_POST['addPATextPaidWTWOTP'],
        'paidCurrOverhaul'     => $_POST['addPATextPaidCurrOverhaul'],
        'paidDate'             => "'".$_POST['addPATextPaidDate']."'",
        'newDebHousingServ'    => $_POST['addPATextNewDebHousingServ'],
        'newDebCurrOverhaul'   => $_POST['addPATextNewDebCurrOverhaul'],
        'numbStorey'           => $_POST['addPATextNumbStorey']
);

$rate = new Rate(true, true, $_POST['nameDB'], $_POST['nameTable']);

if($data['numbStorey'] == 1) {
    $data['housingServRate'] = $rate->housingServRateFirstStorey;
    $data['currOverhaulRate'] = $rate->currOverhaulRateFirstStorey;
}
else {
    $data['housingServRate'] = $rate->housingServRateNotFirstStorey;
    $data['currOverhaulRate'] = $rate->currOverhaulRateNotFirstStorey;
}

$addPA = new AddPersAccount(true, $_POST['nameDB']);
$addPA->addPersAccount($data, $_POST['nameTable']);

header("Location: ".$_SERVER['HTTP_REFERER']);